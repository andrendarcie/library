# Library for SOITIC

To run this project:

- Open it in VS2013
- Select the 'Web' project as startup project.
- Check connection string in web.config and change if you want to.
- Run DB migrations (Run 'Update-Database' command from Package Manager Console while Library.EntityFramework is selected as default project) to create database and initial data.
- Run the application! You will see the login form:
 
Tenancy name: Default

User name: admin

Password: 123qwe
