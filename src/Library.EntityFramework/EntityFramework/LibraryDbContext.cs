﻿using System.Data.Common;
using Abp.Zero.EntityFramework;
using Library.Authorization.Roles;
using Library.MultiTenancy;
using Library.Users;
using System.Data.Entity;
using Library.Books;
using Library.Publishers;
using Library.Authors;

namespace Library.EntityFramework
{
    public class LibraryDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        //TODO: Define an IDbSet for your Entities...
        public virtual IDbSet<Book> Book { get; set; }
        public virtual IDbSet<Publisher> Publisher { get; set; }
        public virtual IDbSet<Author> Author { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public LibraryDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in LibraryDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of LibraryDbContext since ABP automatically handles it.
         */
        public LibraryDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public LibraryDbContext(DbConnection connection)
            : base(connection, true)
        {

        }
    }
}
