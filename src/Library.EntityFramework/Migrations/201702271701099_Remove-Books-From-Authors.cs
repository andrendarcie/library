namespace Library.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveBooksFromAuthors : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BookAuthors", "Book_Id", "dbo.Book");
            DropForeignKey("dbo.BookAuthors", "Author_Id", "dbo.Author");
            DropIndex("dbo.BookAuthors", new[] { "Book_Id" });
            DropIndex("dbo.BookAuthors", new[] { "Author_Id" });
            AddColumn("dbo.Book", "Author_Id", c => c.Guid(nullable: false));
            CreateIndex("dbo.Book", "Author_Id");
            AddForeignKey("dbo.Book", "Author_Id", "dbo.Author", "Id", cascadeDelete: true);
            DropTable("dbo.BookAuthors");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.BookAuthors",
                c => new
                    {
                        Book_Id = c.Guid(nullable: false),
                        Author_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Book_Id, t.Author_Id });
            
            DropForeignKey("dbo.Book", "Author_Id", "dbo.Author");
            DropIndex("dbo.Book", new[] { "Author_Id" });
            DropColumn("dbo.Book", "Author_Id");
            CreateIndex("dbo.BookAuthors", "Author_Id");
            CreateIndex("dbo.BookAuthors", "Book_Id");
            AddForeignKey("dbo.BookAuthors", "Author_Id", "dbo.Author", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BookAuthors", "Book_Id", "dbo.Book", "Id", cascadeDelete: true);
        }
    }
}
