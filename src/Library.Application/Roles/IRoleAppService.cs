﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Library.Roles.Dto;

namespace Library.Roles
{
    public interface IRoleAppService : IApplicationService
    {
        Task UpdateRolePermissions(UpdateRolePermissionsInput input);
    }
}
