﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;

namespace Library.Publishers.Dtos
{
    [AutoMapFrom(typeof(Publisher))]
    public class PublisherDto : FullAuditedEntityDto<Guid>
    {
        public virtual String Nome { get; set; }
        public virtual String Endereco { get; set; }
        public virtual String Telefone { get; set; }
        public int TenantId { get; set; }

        public static PublisherDto MaptoDto(Publisher item)
        {
            var dto = item.MapTo<PublisherDto>();
            dto.Nome = item.Nome;
            dto.Endereco = item.Endereco;
            dto.Telefone = item.Telefone;
            dto.TenantId = item.TenantId;

            return dto;
        }
    }
}
