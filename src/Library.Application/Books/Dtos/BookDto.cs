﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Library.Authors;
using Library.Publishers;
using System;
using System.Collections.Generic;

namespace Library.Books.Dtos
{
    [AutoMapFrom(typeof(Book))]
    public class BookDto : FullAuditedEntityDto<Guid>
    {
        public virtual String Titulo { get; set; }
        public virtual String Isbn { get; set; }
        public virtual String Genero { get; set; }
        public virtual int Edicao { get; set; }
        public virtual int Ano { get; set; }
        public virtual String Idioma { get; set; }
        public virtual Author Author { get; set; }
        public virtual Publisher Publisher { get; set; }
        public int TenantId { get; set; }
        public static BookDto MaptoDto(Book item)
        {
            var dto = item.MapTo<BookDto>();
            dto.Titulo = item.Titulo;
            dto.Isbn = item.Isbn;
            dto.Genero = item.Genero;
            dto.Edicao = item.Edicao;
            dto.Ano = item.Ano;
            dto.Idioma = item.Idioma;
            dto.Author = item.Author;
            dto.Publisher = item.Publisher;
            dto.TenantId = item.TenantId;

            return dto;
        }
    }
}
