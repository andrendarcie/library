﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using System.Linq;
using Abp.Collections.Extensions;
using Library.Books.Dtos;
using Library.Authors;
using Library.Publishers;

namespace Library.Books
{
    public class BookAppService : IBookAppService
    {
        private readonly IBookManage _bookManage;
        private readonly IRepository<Book, Guid> _bookRepository;
        private readonly IRepository<Author, Guid> _authorRepository;
        private readonly IRepository<Publisher, Guid> _publisherRepository;
        private readonly IAbpSession _abpSession;

        public BookAppService(IBookManage bookManage, IRepository<Book, Guid> bookRepository, IRepository<Author, Guid> authorRepository, IRepository<Publisher, Guid> publisherRepository, IAbpSession abpSession)
        {
            _bookManage = bookManage;
            _bookRepository = bookRepository;
            _authorRepository = authorRepository;
            _publisherRepository = publisherRepository;
            _abpSession = abpSession;
        }

        public Task<Book> InsertNewBook(BookDto input)
        {
            // Busca o autor no repositorio
            Author author = _authorRepository.GetAll().FirstOrDefault(x => x.Id == input.Author.Id);

            // Busca a editora no repositorio
            Publisher publisher = _publisherRepository.GetAll().FirstOrDefault(x => x.Id == input.Publisher.Id);

            var book = Book.Create(input.Titulo, input.Isbn, input.Genero, input.Edicao, input.Ano, 
                                        input.Idioma, author, publisher, _abpSession.GetTenantId());
            var bookResult = _bookManage.Create(book);

            return bookResult;
        }

        public BookDto UpdateBook(Book input)
        {
            var bookResult = _bookRepository.Update(input);

            return bookResult.MapTo<BookDto>();
        }

        public void DeleteBook(EntityDto<Guid> input)
        {
            _bookManage.Delete(input.Id);
        }

        public async Task<ListResultDto<BookDto>> GetAllBook()
        {
            var result = await _bookRepository.GetAllListAsync();

            var list = new HashSet<BookDto>();
            foreach (var item in result)
            {
                list.AddIfNotContains(BookDto.MaptoDto(item));
            }

            return new ListResultDto<BookDto>(list.ToList());
        }

        public BookDto GetDetail(EntityDto<Guid> input)
        {
            var result = _bookRepository.GetAll().FirstOrDefault(x => x.Id == input.Id);

            return result.MapTo<BookDto>();
        }
    }
}
