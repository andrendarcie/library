﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Library.Books.Dtos;

namespace Library.Books
{
    public interface IBookAppService : IApplicationService
    {
        Task<Book> InsertNewBook(BookDto input);
        BookDto UpdateBook(Book input);
        void DeleteBook(EntityDto<Guid> input);
        Task<ListResultDto<BookDto>> GetAllBook();
        BookDto GetDetail(EntityDto<Guid> input);
    }
}
