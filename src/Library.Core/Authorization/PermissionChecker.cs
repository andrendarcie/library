﻿using Abp.Authorization;
using Library.Authorization.Roles;
using Library.MultiTenancy;
using Library.Users;

namespace Library.Authorization
{
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
