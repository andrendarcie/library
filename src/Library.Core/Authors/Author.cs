﻿using System;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Library.Books;

namespace Library.Authors
{
    [Table("Author")]
    public class Author : FullAuditedEntity<Guid>, IMustHaveTenant, ISoftDelete
    {
        [Required]
        [StringLength(255)]
        public virtual String FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public virtual String LastName { get; set; }

        public int TenantId { get; set; }

        public static Author Create(string firstName, string lastName, int tenantId)
        {
            var author = new Author
            {
                Id = Guid.NewGuid(),
                FirstName = firstName,
                LastName = lastName,
                TenantId = tenantId,
            };

            return author;
        }
    }
}
