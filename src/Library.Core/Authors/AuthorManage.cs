﻿using Abp.Domain.Repositories;
using System;
using System.Threading.Tasks;

namespace Library.Authors
{
    public class AuthorManage : IAuthorManage
    {
        private readonly IRepository<Author, Guid> _authorReposity;
        public AuthorManage(IRepository<Author, Guid> authorRepository)
        {
            _authorReposity = authorRepository;
        }

        public Task<Author> Create(Author input)
        {
            var authorResult = _authorReposity.InsertAsync(input);
            return authorResult;
        }

        public Task<Author> Update(Author input)
        {
            var author = _authorReposity.UpdateAsync(input);
            return author;
        }

        public void Delete(Guid id)
        {
            _authorReposity.Delete(id);
        }
    }
}
