﻿using Abp.Domain.Services;
using System;
using System.Threading.Tasks;

namespace Library.Authors
{
    public interface IAuthorManage : IDomainService
    {
        Task<Author> Create(Author input);
        Task<Author> Update(Author input);
        void Delete(Guid id);
    }
}
