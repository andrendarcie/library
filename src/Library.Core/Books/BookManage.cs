﻿using Abp.Domain.Repositories;
using System;
using System.Threading.Tasks;

namespace Library.Books
{
    public class BookManage : IBookManage
    {
        private readonly IRepository<Book, Guid> _bookReposity;
        public BookManage(IRepository<Book, Guid> bookRepository)
        {
            _bookReposity = bookRepository;
        }

        public Task<Book> Create(Book input)
        {
            var bookResult = _bookReposity.InsertAsync(input);
            return bookResult;
        }

        public Task<Book> Update(Book input)
        {
            var book = _bookReposity.UpdateAsync(input);
            return book;
        }

        public void Delete(Guid id)
        {
            _bookReposity.Delete(id);
        }
    }
}
