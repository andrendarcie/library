﻿using Abp.Domain.Services;
using System;
using System.Threading.Tasks;

namespace Library.Books
{
    public interface IBookManage : IDomainService
    {
        Task<Book> Create(Book input);
        Task<Book> Update(Book input);
        void Delete(Guid id);
    }
}
