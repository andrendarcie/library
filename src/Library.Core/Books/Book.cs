﻿using System;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Library.Authors;
using System.Collections.Generic;
using Library.Publishers;

namespace Library.Books
{
    [Table("Book")]
    public class Book : FullAuditedEntity<Guid>, IMustHaveTenant, ISoftDelete
    {
        [Required]
        [StringLength(255)]
        public virtual String Titulo { get; set; }

        [Required]
        [StringLength(15)]
        public virtual String Isbn { get; set; }

        [Required]
        [StringLength(15)]
        public virtual String Genero { get; set; }

        [Required]
        public virtual int Edicao { get; set; }

        [Required]
        public virtual int Ano { get; set; }

        [Required]
        [StringLength(15)]
        public virtual String Idioma { get; set; }

        [Required]
        public virtual Author Author { get; set; }

        [Required]
        public virtual Publisher Publisher { get; set; }

        public int TenantId { get; set; }

        public static Book Create(string titulo, string isbn, string genero, int edicao, int ano,
                                    string idioma, Author author, Publisher publisher, int tenantId)
        {
            var book = new Book
            {
                Id = Guid.NewGuid(),
                Titulo = titulo,
                Isbn = isbn,
                Genero = genero,
                Edicao = edicao,
                Ano = ano,
                Idioma = idioma,
                Author = author,
                Publisher = publisher,
                TenantId = tenantId,
            };

            return book;
        }
    }
}
