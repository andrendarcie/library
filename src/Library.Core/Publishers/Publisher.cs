﻿using System;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Library.Publishers
{
    [Table("Publisher")]
    public class Publisher : FullAuditedEntity<Guid>, IMustHaveTenant, ISoftDelete
    {
        [Required]
        [StringLength(255)]
        public virtual String Nome { get; set; }

        [Required]
        [StringLength(255)]
        public virtual String Endereco { get; set; }

        [Required]
        [StringLength(10)]
        public virtual String Telefone { get; set; }

        public int TenantId { get; set; }

        public static Publisher Create(string nome, string endereco, string telefone, int tenantId)
        {
            var publisher = new Publisher
            {
                Id = Guid.NewGuid(),
                Nome = nome,
                Endereco = endereco,
                Telefone = telefone,
                TenantId = tenantId,
            };

            return publisher;
        }
    }
}
