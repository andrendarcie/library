﻿using Abp.Domain.Services;
using System;
using System.Threading.Tasks;

namespace Library.Publishers
{
    public interface IPublisherManage : IDomainService
    {
        Task<Publisher> Create(Publisher input);
        Task<Publisher> Update(Publisher input);
        void Delete(Guid id);
    }
}
