﻿using Abp.Domain.Repositories;
using System;
using System.Threading.Tasks;

namespace Library.Publishers
{
    public class PublisherManage : IPublisherManage
    {
        private readonly IRepository<Publisher, Guid> _publisherReposity;
        public PublisherManage(IRepository<Publisher, Guid> publisherRepository)
        {
            _publisherReposity = publisherRepository;
        }

        public Task<Publisher> Create(Publisher input)
        {
            var publisherResult = _publisherReposity.InsertAsync(input);
            return publisherResult;
        }

        public Task<Publisher> Update(Publisher input)
        {
            var publisher = _publisherReposity.UpdateAsync(input);
            return publisher;
        }

        public void Delete(Guid id)
        {
            _publisherReposity.Delete(id);
        }
    }
}
