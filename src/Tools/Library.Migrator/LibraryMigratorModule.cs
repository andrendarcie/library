using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Library.EntityFramework;

namespace Library.Migrator
{
    [DependsOn(typeof(LibraryDataModule))]
    public class LibraryMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<LibraryDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}