﻿(function () {
    angular.module('app').controller('app.views.publishers.createDialogPublisher', [
        '$uibModalInstance', 'params', '$scope', 'abp.services.app.publisher',
        function ($uibModalInstance, params, $scope, publisherService) {
            var vm = this;

            vm.publisher = {
                id: undefined,
                nome: undefined,
                endereco: undefined,
                telefone: undefined,
                tenantId: undefined
            };

            vm.save = function () {
                if (params.edit === true) {
                    vm.updatePublisher();
                } else {
                    vm.createPublisher();
                }
            };

            vm.createPublisher = function () {
                publisherService.insertNewPublisher(vm.publisher).success(function (data) {
                    abp.notify.success("Editora criada com sucesso");
                    $uibModalInstance.close(true);
                }).error(function (data) {
                    abp.notify.error("Erro ao criar publisher");
                });
            };

            vm.getPublisherDetail = function (item) {
                var input = { id: item }
                publisherService.getDetail(input).success(function (data) {
                    vm.publisher.id = data.id;
                    vm.publisher.nome = data.nome;
                    vm.publisher.endereco = data.endereco;
                    vm.publisher.telefone = data.telefone;
                    vm.publisher.tenantId = data.tenantId;
                }).error(function () {
                    abp.notify.error("Erro ao carregar publisher");
                });
            };

            vm.updatePublisher = function () {
                publisherService.updatePublisher(vm.publisher).success(function (data) {
                    abp.notify.success("Publisher alterada com sucesso");
                    $uibModalInstance.close(true);
                }).error(function (data) {
                    abp.notify.error("Erro ao alterar publisher");
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }

            function init() {
                if (params.edit === true) {
                    vm.getPublisherDetail(params.publisherid);
                }
            };

            init();
        }
    ]);
})();