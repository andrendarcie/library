﻿(function () {
    angular.module('app').controller('app.views.books.createDialogBook', [
        '$uibModalInstance', 'params', '$scope', 'abp.services.app.book', 'abp.services.app.publisher', 'abp.services.app.author',
        function ($uibModalInstance, params, $scope, bookService, publisherService, authorService) {
            var vm = this;

            vm.book = {
                id: undefined,
                titulo: undefined,
                isbn: undefined,
                genero: undefined,
                edicao: undefined,
                ano: undefined,
                idioma: undefined,
                author: undefined,
                publisher: undefined,
                tenantId: undefined
            };

            vm.listPublisher = [];
            vm.listAuthor = [];

            vm.getListPublisher = function () {
                publisherService.getAllPublisher().success(function (result) {
                    vm.listPublisher = result.items;
                    vm.book.publisher = vm.listPublisher[0];
                }).error(function (data) {
                    abp.notify.error("Erro ao carregar lista de editoras");
                });
            };

            vm.getListAuthor = function () {
                authorService.getAllAuthor().success(function (result) {
                    vm.listAuthor = result.items;
                    vm.book.author = vm.listAuthor[0];
                }).error(function (data) {
                    abp.notify.error("Erro ao carregar lista de autores");
                });
            };

            vm.save = function () {
                if (params.edit === true) {
                    vm.updateBook();
                } else {
                    vm.createBook();
                }
            };

            vm.createBook = function () {
                bookService.insertNewBook(vm.book).success(function (data) {
                    abp.notify.success("Livro criado com sucesso");
                    $uibModalInstance.close(true);
                }).error(function (data) {
                    abp.notify.error("Erro ao criar livro");
                });
            };

            vm.getBookDetail = function (item) {
                var input = { id: item }
                bookService.getDetail(input).success(function (data) {
                    vm.book.id = data.id;
                    vm.book.titulo = data.titulo;
                    vm.book.isbn = data.isbn;
                    vm.book.genero = data.genero;
                    vm.book.edicao = data.edicao
                    vm.book.ano = data.ano;
                    vm.book.idioma = data.idioma;
                    vm.book.tenantId = data.tenantId;
                }).error(function () {
                    abp.notify.error("Erro ao carregar livro");
                });
            };

            vm.updateBook = function () {
                bookService.updateBook(vm.book).success(function (data) {
                    abp.notify.success("Livro alterado com sucesso");
                    $uibModalInstance.close(true);
                }).error(function (data) {
                    abp.notify.error("Erro ao alterar livro");
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }

            function init() {
                if (params.edit === true) {
                    vm.getBookDetail(params.bookid);
                } 
                vm.listPublisher = [];
                vm.getListPublisher();
                vm.listAuthor = [];
                vm.getListAuthor();
            };

            init();
        }
    ]);
})();