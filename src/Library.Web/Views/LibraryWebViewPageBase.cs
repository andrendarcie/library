﻿using Abp.Web.Mvc.Views;

namespace Library.Web.Views
{
    public abstract class LibraryWebViewPageBase : LibraryWebViewPageBase<dynamic>
    {

    }

    public abstract class LibraryWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected LibraryWebViewPageBase()
        {
            LocalizationSourceName = LibraryConsts.LocalizationSourceName;
        }
    }
}